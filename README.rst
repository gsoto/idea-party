Idea Party
==========

Develop
-------

Clone the repository::

    git clone ssh://git@gitlab.com/davidism/idea-party
    cd idea-party

Create a virtualenv::

    virtualenv -p python3.5 env
    . env/bin/activate
    pip install -U pip setuptools wheel ipython

Create local settings::

    mkdir instance
    echo "DEBUG = True
    SECRET_KEY = $(python -c 'import os; print(os.urandom(64))')" > instance/settings.py

Install the application in development mode::

    pip install -e .

Create the database::

    idea-party migrate

Run the server::

    idea-party runserver
