from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^register/$', views.register, name='register'),
    url(r'^', include('django.contrib.auth.urls')),
]

app_name = 'idea_party.auth'
