from django.conf.urls import url, include
from django.contrib import admin
from django.views import static
import idea_party.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^static/(?P<path>.*)$', static.serve),
    url(r'^media/(?P<path>.*)$', static.serve),
    url(r'^$', idea_party.views.index, name='index'),
    url(r'^auth/', include('idea_party.auth.urls', namespace='auth')),
]
