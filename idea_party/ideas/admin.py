from django.contrib import admin

from idea_party.ideas.models import Idea

admin.site.register(Idea)
